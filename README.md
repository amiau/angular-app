# Aplicación Angular 5 - 19 Marzo 2018

Este proyecto se llevará a cabo en la plataforma Windows... Linus Torvalds, te he fallado :'(

Si no cuenta con node.js o angular cli instalados:

Lo primero es instalar Node.js [versión 8.10.0 LTS (Long Time Support)](https://nodejs.org/es/)

El msi que se obtiene de la descarga, instala npm (node package manager) y lo añade a la variable PATH para que al ejecutar desde el cmd de Windows, pueda reconocerlo como un comando ejecutable.

![Installing node](md_img/node_install.PNG)

Una vez que la instalación finalice, proceda a instalar angular cli. Abra el command prompt de Node.js y ejecute:

~~~shell
npm install -g @angular/cli
~~~

El flag -g quiere decir que lo instalaremos de manera global. Mayor información, visite el [sitio de angular cli](https://cli.angular.io/)

## Creando la aplicación
Cree una carpeta en la ruta que usted desee y nómbrela con una frase que describa su proyecto. Una vez abierto el cmd de node.js o el cmd de Windows (es indistinto), cambie a la ruta donde se encuentra la carpeta. Una vez en la ruta ejecute:

~~~shell
ng new nombre-del-increible-proyecto
~~~

NOTA: un nombre válido para su proyecto carece de mayúsculas o guiones bajos. Ejemplo:

![Creating a new app](md_img/ng_new.PNG)

Al terminar la ejecución, verá un mensaje similar a: Project nombre-del-increible-proyecto successfully created. Así de simple es crear una aplicación en Angular. 

## Describiendo la estructura de la aplicación
Al revisar el contenido de la carpeta donde almacena su proyecto, verá una carpeta nombre-del-increible-proyecto-app (imagen a continuación). Cambie a esta ruta desde el cmd, es aquí donde trabajará todo el tiempo para crear componentes o servicios nuevos.

![Estructura del proyecto](md_img/estructura.PNG)

La carpeta **e2e** sirve para realizar pruebas (testing), punta a punta (end-to-end), ejecute `ng e2e`. La configuración correspondiente se realiza en **protractor.conf.js** 
Para hacer pruebas unitarias (unit-tests), ejecute `ng test`. La configuración se realiza en **karma.conf.js**

La carpeta **node_modules** contiene todas las dependencias del proyecto, tales como CSS (Bootstrap), xxx o xxxx.

La carpeta **src** contiene tres carpetas importantes: app, assets & environments.
* En la carpeta **app** estarán todos los componentes de la aplicación. Los componentes pueden tratarse como un header o un footer para toda la página, un nav bar, un form, carousel, etc... La ventaja de utilizar Angular, es que cada componente forma una pieza de la aplicación entera y únicamente se manda a llamar en el contenido de la página. (Así lo entiendo)
* En **assets**, incluirá las imágenes que requiera su aplicación.
* En **environments**, hará la declaración de variables que estarán disponibles en el ambiende de desarrollo o producción. Por ejemplo, puede tratarse de las keys de un API del entorno de desarrollo y las de producción. Éstas pueden ser distintas y para evitar cambiarlas cada vez que requiera su uso, se declara cada una en: archivo environment.ts (ambiente de desarrollo), archivo environment.prod.ts (ambiente de producción).

También existen los servicios, los cuales realizan operaciones con verbos HTTP (get, post, put, delete). Tales pueden tratarse del backend, o el consumo a alguna API específica. Se recomienda crear una carpeta en la misma jerarquía donde se encuentran las tres mencionadas anteriormente, con el nombre **core**, dado a que representa una parte importante de la aplicación, pues contiene la lógica del proyecto.

El archivo **angular-cli.json** contiene aquellas importaciones que son ajenas a Angular, principalmente configuraciones.

**NOTA:** si usted ya está familiarizado con el uso de git, se recomienda que en el archivo **.gitignore** se coloque la ubicación de la carpeta node_modules, la cual contiene todas las dependencias del proyecto. De esta manera su proyecto será ligero al clonarlo o descargarlo y sólo bastará con ejecutar `npm i` desde la carpeta raíz de la aplicación para que el descriptor de las dependencias, **package.json**, instale las debidas versiones y la aplicación funcione adecuadamente.

El archivo **.editorconfig** consiste en un formato de archivo para definir estilos de codificación y una colección de complementos de editor de texto que permiten a los editores leer el formato de archivo y adherirse a los estilos definidos. Esto ayuda a los desarrolladores a definir y mantener estilos de codificación coherentes entre diferentes editores e IDE.

tsconfig & tslint; package-lock.json

## Ejecutar su aplicación de manera local
~~~shell
ng serve -o
~~~
Esto abrirá la aplicación en su navegador predeterminado en el puerto por defecto: 4200

## Integrar Bootstrap
Ocuparemos npm para instalar Bootstrap. Cabe mencionar que si desea incluir [glyphicons](https://bootstrapdocs.com/v3.3.6/docs/components/) (íconos propios de este framework) deberá instalar la [versión 3.3.7](https://getbootstrap.com/docs/3.3/getting-started/) en lugar de usar la [versión 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/), la cual dejó de incluir glyphicons. Si prefiere utilizar la versión 4 o tener otras opciones de íconos, se recomienda el uso de [ionicons](http://ionicons.com/cheatsheet.html), [flaticons](https://www.flaticon.com/) o [font awesome](https://fontawesome.com/). Consulte la documentación correspondiente o algunos ejemplos para hacer bien el render.

Una paleta de colores recomendada: [Flat UI Colors 2](https://flatuicolors.com/)

Después de este paréntesis, vaya a la carpeta de la aplicación y ejecute desde la terminal de node.js:
~~~shell
npm install --save bootstrap
~~~
Esto instalará la dependencia de bootstrap dentro de node_modules.

Ahora busque dentro de la carpeta raíz del proyecto en Angular el archivo **angular-cli.json**. Abra este archivo y añada a Bootstrap en el apartado de styles:

~~~json
"styles": [
        "../node_modules/bootstrap/dist/css/bootstrap.min.css",
        "styles.css"
      ]
~~~
Con esto ya ha añadido correctamente Bootstrap en su proyecto. Fuente: [How to include Bootstrap](https://stackoverflow.com/questions/43557321/angular-4-how-to-include-bootstrap)

## Empaquetar para producción
Esto se hace con la finalidad de que la aplicación funcione en el servidor y sea distribuida. Con esto, se crea el bundle o los paquetes donde  estarán presentes los archivos necesarios para ejecutar Angular. Simplemente en la carpeta de la aplicación ejecute:

~~~shell
ng build --prod
~~~

## Typescript
[Aquí](http://www.typescriptlang.org/docs/home.html) se encuentra la documentación para familiarizarse y profundizar en este lenguaje.
el [playground](http://www.typescriptlang.org/play/index.html) le permitirá experimentar con el código ejemplo.

Hola mi amiau
